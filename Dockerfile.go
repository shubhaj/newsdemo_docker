FROM golang:1.14.1

# Enable module management
ENV GO111MODULE=on

RUN mkdir -p /home/newsdemo

COPY .  /home/newsdemo

WORKDIR /home/newsdemo

RUN GOOS=linux GOARCH=amd64 go build main.go

EXPOSE 3000

RUN echo "starting news demo app"

CMD ["sh", "start.sh"]